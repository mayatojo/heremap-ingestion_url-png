import os
import json
import time
import urllib.request
import shutil
import boto3
from datetime import datetime
from urllib.error import HTTPError


# ---------------------------------------------------------- UTIL: UPLOAD TO S3
def upload_to_s3(s3_resource, s3_bucket, s3_location, body):
    """ uploads local file to s3
        s3_resource = boto3.Session().resource('s3')
        s3_location = 'data/HereTraffic/tmp'
    """
    s3_resource.Object(s3_bucket, s3_location).put(Body=body)
    
    
# ------------------------------------------------------------- UTIL: DATE2NAME
def date2name(date, format='YYYY_WXX'):
    """date: datetime.date e.g. datetime.now().date()"""
    if format=='YYYY_DXXX':
        year = str(date.year)
        day = date.strftime('%j')
        name = year + '_D' + day
    elif format=='YYYY_WXX':
        year = str(date.year)
        week = str(date.isocalendar()[1]).zfill(2)
        name = year + '_W' + week
    elif format=='YYYY_MXX':
        year = str(date.year)
        month = str(date.month).zfill(2)
        name = year + '_M' + month
    else:
        print('unknown format, return date as string')
        return str(date)
    return name
    


# -------------------------------------------- UTIL: DOWNLOAD AND SAVE LOCAL/S3
# save on s3
def download_to_s3(url, s3_resource, s3_bucket, s3_location):
    response = urllib.request.urlopen(url)
    uploadByteStream = response.read()  # bytes object
    upload_to_s3(s3_resource, s3_bucket, s3_location, body=uploadByteStream)


# ------------------------------------------------------------------------ MAIN

s3_client = boto3.client('s3')
s3_resource = boto3.Session().resource('s3')
s3_location = '1_RawData/Traffic_HereMaps/' + date2name(datetime.now().date(), format='YYYY_MXX') + '/'
s3_bucket = 'ev-modeling-platform'

urls_bytes = s3_client.get_object(Bucket=s3_bucket,
                            Key=s3_location + 'urls.json')
file_content = urls_bytes['Body'].read().decode('utf-8')
urls = json.loads(file_content)

max_short_breaks = 3
max_long_breaks = 3
download_break_time = 1.5
short_break_time = 10
long_break_time = 5*60
downloads_since_last_sleep = 0
for key in urls:
    name = key
    url = urls[key]
    count_long_break = 0
    count_short_break = 0
    while True:
        try:
            download_to_s3(url, s3_resource, s3_bucket, s3_location=s3_location+name)
            time.sleep(download_break_time)
            downloads_since_last_sleep += 1
            break
        except HTTPError as err:
            if err.code in [500, 503, 504]:
                if count_short_break < max_short_breaks:
                    print(
                        'HTTPError 504, sleep 10 seconds. '
                        'Download since last sleep ', downloads_since_last_sleep)
                    count_short_break += 1
                    downloads_since_last_sleep = 0
                    time.sleep(short_break_time)
                else:
                    if count_long_break < max_long_breaks:
                        print('Need longer break, sleep 2 minutes. '
                              'Download since last sleep ', downloads_since_last_sleep)
                        count_long_break += 1
                        success_downloads = 0
                        downloads_since_last_sleep = 0
                        time.sleep(long_break_time)
                    else:
                        print(
                            'Cannot have a stable connection, stop sending requests')
                        raise
            else:
                print('Unknown Error.')
                raise  # different connection error