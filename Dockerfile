FROM python:3


COPY requirements.txt ./
RUN pip freeze > requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install boto3

RUN mkdir /src

COPY HereTraffic_Download.py /src/HereTraffic_Download.py

CMD ["python", "/src/HereTraffic_Download.py"]